<?php

function wd_load_script() {
    wp_deregister_script( 'jquery' );
    wp_register_script( 'jquery', get_template_directory_uri() . '/js/jquery-1.7.2.min.js');
 
    wp_enqueue_script( 'jquery' );
}    
 
add_action('wp_enqueue_scripts', 'wd_load_script');

/*------------------------------------------------------------*/
/*   Registrar Menus WP3.0+
/*------------------------------------------------------------*/
if ( function_exists( 'register_nav_menus' ) )
{
	register_nav_menus(
    	array(
        	
        )
    );
}

if ( function_exists('register_sidebar') )
    register_sidebar();