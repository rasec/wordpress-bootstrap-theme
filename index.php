<?php get_header(); ?>

	<div id="content" class="narrowcolumn">
		<div class="row-fluid">
			<?php if (have_posts()) : ?>
				<section class="span9">
				<?php while (have_posts()) : the_post(); ?>

					<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
						<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php printf(__('Permanent Link to %s', 'kubrick'), the_title_attribute('echo=0')); ?>"><?php the_title(); ?></a></h2>
						
						<!-- story header -->
				        <div class="postheader">
				          <div class="postinfo">
				            <p><?php printf(__('Posted by %s in %s on %s','fusion'),'<a href="'. get_author_posts_url(get_the_author_ID()) .'" title="'. sprintf(__("Posts by %s","fusion"), attribute_escape(get_the_author())).' ">'. get_the_author() .'</a>',get_the_category_list(', '), get_the_time(get_option('date_format'))); ?> <?php edit_post_link(__('Edit','fusion'),'| ',''); ?></p>
				          </div>
				        </div>
				        <!-- /story header -->

						<div class="entry">
							<?php the_content(__('Read the rest of this entry &raquo;', 'kubrick')); ?>
						</div>

						<p class="postmetadata">
							<?php the_tags(__('Tags:', 'kubrick') . ' ', ', ', ''); ?> 
							<span class="right"><?php comments_popup_link(__('No Comments &#187;', 'kubrick'), __('1 Comment &#187;', 'kubrick'), __('% Comments &#187;', 'kubrick'), '', __('Comments Closed', 'kubrick') ); ?></span>
						</p>
					</article>

				<?php endwhile; ?>

				<div class="navigation pager">
					<ul>
					<li class="previous"><?php next_posts_link(__('&laquo; Older Entries', 'kubrick')) ?></li>
					<li class="next"><?php previous_posts_link(__('Newer Entries &raquo;', 'kubrick')) ?></li>
				</div>

			<?php else : ?>

				<h2 class="center"><?php _e('Not Found', 'kubrick'); ?></h2>
				<p class="center"><?php _e('Sorry, but you are looking for something that isn&#8217;t here.', 'kubrick'); ?></p>
				<?php include (TEMPLATEPATH . "/searchform.php"); ?>

			<?php endif; ?>

			</section>
			<aside class="span3">
				<?php get_sidebar(); ?>
			</aside>
		</div>
	</div>

<?php get_footer(); ?>
