<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="alternate" type="application/rss+xml" title="<?php printf(__('%s RSS Feed', 'kubrick'), get_bloginfo('name')); ?>" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php printf(__('%s Atom Feed', 'kubrick'), get_bloginfo('name')); ?>" href="<?php bloginfo('atom_url'); ?>" /> 
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/bootstrap.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/prettify/prettify.js"></script>


<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?> 

<?php wp_head(); ?>
</head>
<body class="container">
	<nav id="tabs" class="navbar navbar-fixed-top navbar-inverse">
		<div class="navbar-inner">
			<div class="container">
	         	<ul class="nav">
			         <?php
			          if((get_option('show_on_front')<>'page') && (get_option('bootstrap_topnav')<>'categories')) {
			          	if(is_home()){ ?>
			            <li id="nav-homelink" class="current_page_item active"><a href="<?php echo get_settings('home'); ?>" title="<?php _e('You are Home','bootstrap'); ?>"><span><span><?php _e('Home','bootstrap'); ?></span></span></a></li>
			           <?php } else { ?>
			            <li id="nav-homelink" >
			            	<a href="<?php echo get_option('home'); ?>" title="<?php _e('Click for Home','bootstrap'); ?>"><span><span><?php _e('Home','bootstrap'); ?></span></span></a></li>
			          <?php
			           }
			          } ?>
			         <?php
			           if(get_option('bootstrap_topnav')=='categories') { echo preg_replace('@\<li([^>]*)>\<a([^>]*)>(.*?)\<\/a>@i', '<li$1><a$2><span><span>$3</span></span></a>', wp_list_categories('show_count=0&echo=0&title_li='));  }
			           else { echo preg_replace('@\<li([^>]*)>\<a([^>]*)>(.*?)\<\/a>@i', '<li$1><a$2><span><span>$3</span></span></a>', wp_list_pages('echo=0&title_li=&')); }
			          ?>
			    </ul>
			    <div class="nav-collapse">
					<?php get_search_form(); ?>
				</div>
			    
			</div>
		</div>
	</nav>

	<header id="overview" class="jumbotron subhead page-header">
		<div id="headerimg">
			<h1><a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a></h1>
			<div class="description"><?php bloginfo('description'); ?></div>
		</div>
	</header>
